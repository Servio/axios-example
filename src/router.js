import Vue from 'vue'
import Router from 'vue-router'
import TryAxios from './components/TryAxios'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'TryAxios',
      component: TryAxios
    }
  ]
})
